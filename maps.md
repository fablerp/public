# Maps

The only repo that matters

## Maps

| Name | Resource | Path | Description |
| --- | --- | --- | --- |
| UWU Cafe | gabz_catcafe | gabzcat_cafe | UWU Cat Cafe |
| Ballas Interior | maps1 | maps1/stream/[ballas] | Ballas Gang Interior |
| Blackwood Saloon | maps1 | maps1/stream/[blackwood saloon] | Blackwood Saloon, replaces Hen House in Paleto |
| Bobo Prison | maps1 | maps1/stream/[bobo_prison] | Prison interior |
| City Hall | maps1 | maps1/stream/[cityhall] | City Hall interior |
| Davis Station | maps1 | maps1/stream/[davispd] | Davis Sheriff Station interior |
| Car Dealer | maps1 | maps1/stream/[dealer_map] | Car Dealership |
| Mirror Park Tavern | maps1 | maps1/stream/[dons mp tavern] | Tavern in "Main Street" Mirror Park |
| Tim Hortons | maps1 | maps1/stream/[dons timmy hortons] | Tim Hortons around corner from Misson Row |
| Driving School | maps1 | maps1/stream/[drivingschool] | Driving School interior |
| Fight Club | maps1 | maps1/stream/[fightclub] | Flight Club interior |
| Lost Compound | maps1 | maps1/stream/[gabz lost] | Lost Compound, Mirror Park |
| Mission Row Police | maps1 | maps1/stream/[gabz mrpd] | Gabz' Mirror Park Police Station |
| Pacific Standard Bank | maps1 | maps1/stream/[gabz pacstd] | Gabz' Pacific Standard Bank |
| Hotdog Stand | maps1 | maps1/stream/[hotdog] | Hotdog interior |
| Hunting Shop | maps1 | maps1/stream/[hunting-shop] | Hunting Shop Interior |
| Weed Shop | maps1 | maps1/stream/[int_weed] | Weed interior |
| Courthouse | maps1 | maps1/stream/[j17 courthouse] | Courthouse across highway from Pillbox |
| Jail Fence | maps1 | maps1/stream/[jail fence] | YMAP adding fence to BBP yard |
| La Mesa | maps1 | maps1/stream/[lamesa] | La Mesa Police Station interior |
| Legion Sign | maps1 | maps1/stream/[legion sign] | Ron sign near Legion |
| Los Santos Customs | maps1 | maps1/stream/[lsc] | Custom LSC interior |
| Mount Zonah | maps1 | maps1/stream/[mt zonah] | Mt Zonah Hospital interior |
| Noodle Exchange | maps1 | maps1/stream/[noodle exchange] | Noodle Exchange Restaruant Interior |
| Paleto Medical | maps1 | maps1/stream/[paleto med] | Paleto Medical Center Interior |
| Paleto Sheriff | maps1 | maps1/stream/[paleto pd] | Paleto Sheriff Station Interior |
| Pearls | maps1 | maps1/stream/[pearls] | Pearls Restaruant and Bar interiors |
| Rebel | maps1 | maps1/stream/[rebel] | Rebel Radio interior |
| Rockford Gas | maps1 | maps1/stream/[rockfordgas] | Rockford Gas store interior |
| Sandy Hospital | maps1 | maps1/stream/[sandy]/[hosp] | Sandy Shores Medical Center interior |
| Sandy Sheriff Station | maps1 | maps1/stream/[sandy]/[pd] | Sandy Shores Sheriff Station interior |
| Taco Stand Interior | maps1 | maps1/stream/[taco] | Taco stand in Davis interior |
| Taxi | maps1 | maps1/stream/[taxi] | Taxi interior in Mirror Park |
| Trees | maps1 | maps1/stream/[trees] | Tree ymaps for Harmony north to Paleto |
| Vineyard | maps1 | maps1/stream/[vine] | Vineyard map |
| Vanilla Unicorn | maps1 | maps1/stream/[vunicorn] | Vanilla Unicorn Map |
| Weasel News | maps1 | maps1/stream/[weasel news] | Weasel News interior |

## Scenarios

| Name | Type | Path | Description/Changes |
| --- | --- | --- | --- |
| Davis | Replace | scenarios/stream/davis.ymt | Remove helicopter spawn on Davis station |
| South Los Santos | Replace | scenarios/stream/south_los_santos.ymt | Remove helicopter spawns at CLS |
| Strawberry | Replace | scenarios/stream/strawberry.ymt | Increase height of police helicopter that orbits over Grove |