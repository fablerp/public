# Law Enforcement Vehicles

## LSPD

### Vehicles

| Model Name       | Description                                                                                           |
| ---------------- | ----------------------------------------------------------------------------------------------------- |
| *Retro Pack*                                                                                                             |
| lspd49custom     | LSPD Classic - 1949 Ford Custom w/ Beacon light (M1)                                                  |
| *Liberty II Pack* (Lightbar [Liberty II] and Visor Lights as extras)                                                     |
| lspd11cvpi       | LSPD - 2011 Ford CVPI (M)                                                                             |
| lspd16fpiu       | LSPD - 2016 Ford FPIU (M/UM)                                                                          |
| lspd18charger    | LSPD - 2018 Dodge Charger (M/UM)                                                                      |
| lspd18fpis       | LSPD - 2018 Ford FPIS (M/UM)                                                                          |
| lspd19tahoe      | LSPD - 2019 Chevy Tahoe (M/UM)                                                                        |

### Aviation/Boats

| Model Name       | Type          | Description                                                                           |
| ---------------- | ------------- | ------------------------------------------------------------------------------------- |
| lspdec135        | Helicopter    | Eurocopter EC135 (needs reskinning)                                                   |

## LSSD

### Vehicles

| Model Name       | Description                                                                                           |
| ---------------- | ----------------------------------------------------------------------------------------------------- |
| lssd49custom     | LSSD Classic - 1949 Ford Custom w/ Beacon Light (M1)                                                  |
| ---------------- | ----------------------------------------------------------------------------------------------------- |
| *Legacy Pack* (Lightbar [Whelen Legacy] and Visor Lights as extras)                                                      |
| ---------------- | ----------------------------------------------------------------------------------------------------- |
| lssd11cvpi       | LSSD - 2011 Ford Crown Victoria (M)                                                                   |
| lssd18charger    | LSSD - 2018 Dodge Charger (M/UM)                                                                      |
| lssd18fpis       | LSSD - 2018 Ford FPIS (M)                                                                             |
| lssd19tahoe      | LSSD - 2019 Chevy Tahoe (M)                                                                           |
| lssd20fpiu       | LSSD - 2020 Ford FPIU (M/UM)                                                                          |
| lssd21durango    | LSSD - 2021 Dodge Durango (M)                                                                         |

### Aviation/Boats

| Model Name       | Type          | Description                                                                            |
| ---------------- | ------------- | -------------------------------------------------------------------------------------- |
| polmav           | Helicopter    | Templated Polmav                                                                       |

## Notes

Marking key:
* M = Marked
* M1 = Marked, old/classic livery
* S = Stealth
* UM = Unmarked
