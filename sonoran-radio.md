# SonoranRadio and TS Info

Teamspeak Server: ts.fable.city
Password: ask devs

## Channels

| Name | Type | Freq Rx | Freq Tx | Description |
| ---- | ---- | ---- | ----- | ----- |
| Blue 1 | Dispatch | 45.445 | 45.42 | Primary dispatch/UNICOM |
| Red 1 | Dispatch | 158.995 | 158.97 | EMS/Fire Dispatch |
| Orange 1 | Dispatch | 42.545 | 45.52 | LSPD Dispatch |
| Orange 2 | Tactical | 42.465 | 42.44 | LSPD Tac 1 |
| Green 1 | Dispatch | 39.385 | 39.36 | BCSO Dispatch |
| Green 2 | Tactical | 42.245 | 42.22 | BCSO Tac 1 |
| Bronze 1 | Special | 155.975 | 155.95 | Academy |
| Talk Around 1 | Talk Around | 172.681 | - | Talk Around, 1km range |
| Talk Around 2 | Talk Around | 172.181 | - | Talk Around, 1km range |

## Why these names?

In CA, many agencies use colors to designate channel names. It makes inter-agency communication a little easier 
as a unit need only say "Dispatch, 907, switching to green 1" rather than "Dispatch, 907, switching to BCSO Dispatch".

The colors are used to break up sections. Blue is primary dispatch or UNICOM, when dispatchers need to separate by region, they can open Red (fire), 
Orange (city) or Green (BCSO). 1 will always be dispatch, 2+ are special channels usually tactical for use in special missions, etc.

Talk Around channels are meant to be used as car-to-car and will more often than not, not have dispatch available due to limited range and simplex operations.
