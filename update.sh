#!/bin/bash

update_path=$1

# if not passed, set default (Frozen's dev directory)
if [[ -z "$update_path" ]]; then
  update_path="/d/fivem/txData/CFXDefault_1ED201.base/resources"
fi

function copyfile() {
  from=$1
  to=$2
  echo "Copying $update_path/$from to $to"
  cp "$update_path/$from" "$to"
}

copyfile "[vehicles]/ems_pack/README.md" vehicles-ems.md
copyfile "[vehicles]/police_pack/README.md" vehicles-leo.md
copyfile "[vehicles]/civ_pack/README.md" vehicles-civ.md
copyfile "[maps]/README.md" maps.md
cp ../readme/sonoran-radio.md sonoran-radio.md
