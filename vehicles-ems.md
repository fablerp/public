# EMS Vehicles

## Vehicles

| Model Name       | Description                                                                                           |
| ---------------- | ----------------------------------------------------------------------------------------------------- |
| e350medic        | Vanbulance                                                                                            |

## Aviation/Boats

| Model Name       | Type          | Description                                                                           |
| ---------------- | ------------- | ------------------------------------------------------------------------------------- |
| lspdec135        | Helicopter    | Eurocopter EC135                                                                      |